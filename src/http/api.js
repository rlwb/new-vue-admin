import { http } from './index'
import { Message } from 'element-ui'

//登录方法
export function login(data) {
    return http('login', 'POST', data).then(res =>{
      console.log(res)
      Message({
          showClose: true,
        message: res.meta.msg + res.data.username,
          type: 'success'
      })
      return res.data
    })
}
//获取权限列表

export function getMenus() {
    return http('menus', 'GET').then(res => {
        return res.data
    })
}

//获取用户列表

export function getUsers(params) {
  return http('users', 'GET', {}, params).then(res => {
    return res.data
  })
}

//添加用户
export function addUsers(data) {
  return http('users', 'POST', data).then(res => {
    Message({
        showClose: true,
        message: res.meta.msg,
        type: 'success'
    })
    return res.data
  })
}

//修改用户状态
export function changeUserStatus(uId, type) {
  return http(`users/${uId}/state/${type}`, 'PUT').then(res => {
    Message({
        showClose: true,
        message: res.meta.msg,
        type: 'success'
    })
    return res.data
  })
}

//编辑用户提交

export function editUser(id,data) {
  return http(`users/${id}`, 'PUT', data).then(res =>{
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//删除用户
export function deleteUser(id) {
  return http(`users/${id}`, 'DELETE').then(res => {
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//获取角色列表

export function getRolesLists() {
  return http('roles').then(res => {
    return res.data
  })
}

//获取用户角色id-rid

export function getRidById(id) {
  return http(`users/${id}`).then(res => {
    return res.data
  })
}

//分配角色

export function deliverRole(id, rid) {
  return http(`users/${id}/role`,'PUT', {
    rid
  }).then(res => {
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//删除权限通过id
export function deleteRight(roleId, rightId) {
  return http(`roles/${roleId}/rights/${rightId}`, 'DELETE').then(res => {
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//获取权限列表树

export function getAllRightsTree() {
  return http(`rights/tree`).then(res => {
    return res.data
  })
}


//角色授权
export function editRightIds(roleId, rids) {
  return http(`roles/${roleId}/rights`, 'POST', {
    rids
  }).then(res => {
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//添加角色

export function addRoles(data) {
  return http('roles', 'POST', data).then(res => {
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//编辑角色
export function editRoles(data) {
  return http('roles/'+data.id, 'PUT',data).then(res=>{
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//删除角色
export function deleteRoles(id) {
  console.log(id);
  return http('roles/' + id, 'DELETE').then(res => {
    Message({
      showClose: true,
      message: res.meta.msg,
      type: 'success'
    })
    return res.data
  })
}

//获取权限列表list
export function getRightsLists() {
  return http('rights/list').then(res => {
    return res.data
  })
}

//获取商品列表
export function getGoodsLists(params) {
  return http('goods', 'GET', {}, params).then(res => {
    return res.data
  })
}