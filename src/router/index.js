import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },{
    path: '/index',
    name: 'index',
    component: () => import(/* webpackChunkName: "index" */ '../views/Index.vue'),
    children: [
      {
        path: 'roles',
        name: 'roles',
        component: () => import(/* webpackChunkName: "roles" */ '../views/roles/Roles.vue')
      },
      {
        path: 'users',
        name: 'users',
        component: () => import(/* webpackChunkName: "users" */ '../views/users/Users.vue')
      },
      {
        path: 'rights',
        name: 'rights',
        component: () => import(/* webpackChunkName: "rights" */ '../views/rights/Rights.vue')
      },
      {
        path: 'goods',
        name: 'goods',
        component: () => import(/* webpackChunkName: "goods" */ '../views/goods/GoodsLists.vue')
      }
    ]
  },
  {
      path: '/index',
      redirect: '/index/users',
      name: 'redirect_users'
  },
]

const router = new VueRouter({
  routes
})

export default router
