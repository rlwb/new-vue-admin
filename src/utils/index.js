export function debounce(fn, delay) {
    let timer
    return (...args) => {
       clearTimeout(timer)
       timer = setTimeout(()=>{
            fn.apply(this, args)
        }, delay)
    }
}


export function throttle(fn, delay) {
    let flag = true
    return (...args) => {
        if(!flag) return;
        flag = false
        setTimeout(() => {
            fn.apply(this, args)
            flag = true
        }, delay);
    }
}